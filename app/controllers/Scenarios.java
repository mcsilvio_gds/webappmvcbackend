package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.Map;

import models.Campaign;
import models.CampaignScenario;
import models.Event;
import models.GlobalConstraintSet;
import models.Scenario;
import play.data.Form;
import play.mvc.*;
import views.html.*;

public class Scenarios extends Controller {

    public static final Long NEW_ID = (long) -1;

    public static Result index(Long event_id) {
        // get Event
        Event event = Event.find.byId(event_id);

        List<Scenario> scens = event.scenarios;

        return ok(views.html.scenario.index.render(scens, event_id));
    }

   

    /**
     * Displays the details of a single Event
     */
    public static Result view(Long event_id, Long id) {
        // get the scenario
        Scenario scen = Scenario.find.byId(id);
        
        //get its campaign constraints
        List<CampaignScenario> campaignCons = scen.campaignScenarios; 

        // render view
        return ok(views.html.scenario.view.render(scen, event_id, campaignCons));
    }

    /**
     * Displays a form for adding a new Event
     */
    public static Result add(Long event_id) {
        
        //get event
        Event event = Event.find.byId(event_id);

        //create new Scenario
        Scenario scenario = new Scenario();
        scenario.event = event;
        
        // create blank form
        Form<Scenario> scenarioForm = form(Scenario.class).fill(scenario);

        // render view
        return ok(views.html.scenario.scenario.render(scenarioForm, event_id, NEW_ID));
    }

    /**
     * Displays a form that is populated with the given Event for updating.
     * 
     * @param id
     *            the id of the Event to update
     */
    public static Result update(Long event_id, Long id) {
        // get event
        Scenario scenario = Scenario.find.byId(id);

        // populate form with found Event
        Form<Scenario> scenarioForm = form(Scenario.class).fill(scenario);

        // render view
        return ok(views.html.scenario.scenario.render(scenarioForm, event_id, scenario.id));
    }

    /**
     * This action handles the submit POST request. Using the given Event ID, it
     * will determing whether or not this is for a NEW Event, or for updating an
     * EXISTING Event.
     */
    public static Result submit(Long event_id) {

        // grab form and populate it from POST request
        Form<Scenario> submittedForm = form(Scenario.class).bindFromRequest();

        // get the Event ID
        Map<String, String> map = submittedForm.data();
        Long id = Long.parseLong(map.get("scenario_id"));

        // keep two events, but only save 'eventToSave'
        Scenario scenarioFromForm = submittedForm.get();
        Scenario scenarioToSave = null;

        // if ID indicates a new event is needed, use the form's Event
        if(id == NEW_ID) {
            scenarioToSave = scenarioFromForm;            
        }
        // if ID indicates an update is required, use existing Event
        else {
            scenarioToSave = Scenario.find.byId(id);
            scenarioToSave.name = scenarioFromForm.name;
            scenarioToSave.description = scenarioFromForm.description;
        }

        // save Event
        scenarioToSave.save();
        
        // if ID indicates a new event is needed, use the form's Event
        if(id == NEW_ID) {
            
            //get and assign event
            Event event = Event.find.byId(event_id);
            scenarioToSave.event = event;
            
            //create a new globalConstraintSet
            GlobalConstraintSet gCons = new GlobalConstraintSet();
            gCons.scenario = scenarioToSave;
            scenarioToSave.globalConstraintSet = gCons;
            gCons.save();
            
            //create a new campaign_scenario for each campaign in the event
            List<Campaign> campaigns = event.campaigns;
            for(Campaign campaign: campaigns){
                CampaignScenario campScen = new CampaignScenario();
                campScen.campaign = campaign;
                campScen.scenario = scenarioToSave;
                campScen.save();
            }
        }
        
        scenarioToSave.save();

        // redirect to index
        return redirect(routes.Scenarios.view(event_id, scenarioToSave.id));

    }

    /**
     * This action handles deleting the given Event.
     * 
     * @param id
     *            The id of the Event to delete
     */
    public static Result delete(Long event_id, Long id) {

        Scenario scenario = Scenario.find.byId(id);
        scenario.delete();

        return redirect(routes.Events.view(event_id));
    }
}
