package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.Map;

import models.Campaign;
import models.Event;
import models.GlobalConstraintSet;
import models.Scenario;
import play.data.Form;
import play.mvc.*;
import views.html.*;

public class Campaigns extends Controller {

    public static final Long NEW_ID = (long) -1;

    public static Result index(Long event_id) {
        // get Event
        Event event = Event.find.byId(event_id);

        List<Campaign> campaigns = event.campaigns;

        return ok(views.html.campaign.index.render(campaigns, event_id));
    }

    /**
     * Displays the details of a single Event
     */
    public static Result view(Long event_id, Long id) {
        // get all events
        Campaign campaign = Campaign.find.byId(id);

        // render view
        return ok(views.html.campaign.view.render(campaign, event_id));
    }

    /**
     * Displays a form for adding a new Event
     */
    public static Result add(Long event_id) {
        
        // create blank form
        Form<Campaign> campaignForm = form(Campaign.class);

        // render view
        return ok(views.html.campaign.campaign.render(campaignForm, event_id, NEW_ID));
    }

    /**
     * Displays a form that is populated with the given Event for updating.
     * 
     * @param id
     *            the id of the Event to update
     */
    public static Result update(Long event_id, Long id) {
        // get event
        Campaign campaign = Campaign.find.byId(id);

        // populate form with found Event
        Form<Campaign> campaignForm = form(Campaign.class).fill(campaign);

        // render view
        return ok(views.html.campaign.campaign.render(campaignForm, event_id, campaign.id));
    }

    /**
     * This action handles the submit POST request. Using the given Event ID, it
     * will determing whether or not this is for a NEW Event, or for updating an
     * EXISTING Event.
     */
    public static Result submit(Long event_id) {

        // grab form and populate it from POST request
        Form<Campaign> submittedForm = form(Campaign.class).bindFromRequest();

        // get the Event ID
        Map<String, String> map = submittedForm.data();
        Long id = Long.parseLong(map.get("campaign_id"));

        // keep two events, but only save 'eventToSave'
        Campaign campaignFromForm = submittedForm.get();
        Campaign campaignToSave = null;

        // if ID indicates a new event is needed, use the form's Event
        if(id == NEW_ID) {
            campaignToSave = campaignFromForm;
            
            //get and assign event
            Event event = Event.find.byId(event_id);
            campaignToSave.event = event;            
        }
        // if ID indicates an update is required, use existing Event
        else {
            campaignToSave = Campaign.find.byId(id);
            campaignToSave.name = campaignFromForm.name;
        }

        // save Event
        campaignToSave.save();

        // redirect to index
        return redirect(routes.Events.view(event_id));

    }

    /**
     * This action handles deleting the given Event.
     * 
     * @param id
     *            The id of the Event to delete
     */
    public static Result delete(Long event_id, Long id) {

        Campaign campaign = Campaign.find.byId(id);
        campaign.delete();

        return redirect(routes.Events.view(event_id));
    }
}
