package controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Campaign;
import models.Event;
import models.Scenario;
import play.*;
import play.data.*;
import static play.data.Form.*;
import play.mvc.*;
import views.html.*;

public class Events extends Controller {

    public static final Long NEW_ID = (long) -1;

    /**
     * Displays all of the events, and provides links to other actions
     */
    public static Result index() {
        //get all events
        List<Event> events = Event.find.all();
        
        //render view
        return ok(views.html.event.index.render(events));
    }
    
    /**
     * Displays the details of a single Event
     */
    public static Result view(Long id) {
        //find event
        Event event = Event.find.byId(id);
        
        //get its scenarios
        List<Scenario> scenarios = event.scenarios;
        
        //get its campaigns
        List<Campaign> campaigns = event.campaigns;
        
        //render view
        return ok(views.html.event.view.render(event, scenarios, campaigns));
    }

    /**
     * Displays a form for adding a new Event
     */
    public static Result add() {

        //create blank form
        Form<Event> eventForm = form(Event.class);

        //render view
        return ok(views.html.event.event.render(eventForm, NEW_ID));
    }

    /**
     * Displays a form that is populated with the 
     * given Event for updating.
     * @param id the id of the Event to update
     */
    public static Result update(Long id) {
        // get event
        Event event = Event.find.byId(id);

        // populate form with found Event
        Form<Event> eventForm = form(Event.class).fill(event);

        //render view
        return ok(views.html.event.event.render(eventForm, event.id));
    }

    /**
     * This action handles the submit POST request. Using the given Event ID,
     * it will determing whether or not this is for a NEW Event, or for updating 
     * an EXISTING Event.
     */
    public static Result submit() {

        //grab form and populate it from POST request
        Form<Event> submittedForm = form(Event.class).bindFromRequest();

        //get the Event ID
        Map<String, String> map = submittedForm.data();
        Long id = Long.parseLong(map.get("event_id"));

        //keep two events, but only save 'eventToSave'
        Event eventFromForm = submittedForm.get();
        Event eventToSave = null;
        
        //if ID indicates a new event is needed, use the form's Event
        if(id == NEW_ID) {
            eventToSave = eventFromForm;
        }
        //if ID indicates an update is required, use existing Event
        else{
            eventToSave = Event.find.byId(id);
            eventToSave.name = eventFromForm.name;
            eventToSave.description = eventFromForm.description;
        }
        
        //save Event
        eventToSave.save();

        //redirect to index
        return redirect(routes.Events.view(eventToSave.id));

    }

    /**
     * This action handles deleting the given Event.
     * @param id The id of the Event to delete
     */
    public static Result delete(Long id) {

        Event event = Event.find.byId(id);
        event.delete();

        return redirect(routes.Events.index());
    }
    
    //helper class to populate event table if schema changes
    public static Result createdb() {
        for(int i = 0; i < 8; i++) {
            Event ev = new Event();
            ev.name = "oink" + i;
            ev.save();
        }

        return ok("done");
    }

}