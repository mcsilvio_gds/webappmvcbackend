package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.Map;

import models.Event;
import models.GlobalConstraintSet;
import models.Scenario;
import play.data.Form;
import play.mvc.*;
import views.html.*;

public class GlobalConstraints extends Controller {

    public static final Long NEW_ID = (long) -1;
    
    /**
     * Displays the details of a single Event
     */
    public static Result view(Long constraint_id) {
        //get constraint set
        GlobalConstraintSet gCons = GlobalConstraintSet.find.byId(constraint_id);

        // render view
        return ok(views.html.globalConstraints.view.render(gCons));
    }

    

    /**
     * Displays a form that is populated with the given Event for updating.
     * 
     * @param id
     *            the id of the Event to update
     */
    public static Result update(Long constraint_id) {
        
        //get constraint set
        GlobalConstraintSet gCons = GlobalConstraintSet.find.byId(constraint_id);

        // populate form with found Event
        Form<GlobalConstraintSet> gConsForm = form(GlobalConstraintSet.class).fill(gCons);

        // render view
        
        return ok(views.html.globalConstraints.globalConstraints.render(gConsForm, constraint_id));
    }

    /**
     * This action handles the submit POST request. Using the given Event ID, it
     * will determing whether or not this is for a NEW Event, or for updating an
     * EXISTING Event.
     */
    public static Result submit() {

        // grab form and populate it from POST request
        Form<GlobalConstraintSet> submittedForm = form(GlobalConstraintSet.class).bindFromRequest();

        // get the real ID
        Map<String, String> map = submittedForm.data();
        Long id = Long.parseLong(map.get("constraint_id"));
        
        //get constraints and save
        GlobalConstraintSet constraintsFromForm = submittedForm.get();
        GlobalConstraintSet constraintsToSave = GlobalConstraintSet.find.byId(id);
        
        constraintsToSave.name = constraintsFromForm.name;
        constraintsToSave.save();
        

        // redirect to index
        //return ok(views.html.globalConstraints.print.render(constraintsFromForm.id));
        return redirect(routes.Scenarios.view(constraintsToSave.scenario.event.id, constraintsToSave.scenario.id));

    }
}
