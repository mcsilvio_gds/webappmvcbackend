package controllers;

import static play.data.Form.form;

import java.util.List;
import java.util.Map;

import models.Campaign;
import models.CampaignScenario;
import models.Event;
import models.GlobalConstraintSet;
import models.Scenario;
import play.data.Form;
import play.mvc.*;
import views.html.*;

public class CampaignScenarios extends Controller {

    public static final Long NEW_ID = (long) -1;

    /**
     * Displays the details of a single Event
     */
    public static Result view(Long campaignCon_id) {
        //get constraint set
        CampaignScenario campaignCons = CampaignScenario.find.byId(campaignCon_id);

        // render view
        return ok(views.html.campaignScenario.view.render(campaignCons));
    }

    

    /**
     * Displays a form that is populated with the given Event for updating.
     * 
     * @param id
     *            the id of the Event to update
     */
    public static Result update(Long campaignCon_id) {
        
        //get constraint set
        CampaignScenario campaignCons = CampaignScenario.find.byId(campaignCon_id);

        // populate form with found Event
        Form<CampaignScenario> campaignConsForm = form(CampaignScenario.class).fill(campaignCons);

        // render view
        
        return ok(views.html.campaignScenario.campaignScenario.render(campaignConsForm, campaignCon_id));
    }

    /**
     * This action handles the submit POST request. Using the given Event ID, it
     * will determing whether or not this is for a NEW Event, or for updating an
     * EXISTING Event.
     */
    public static Result submit() {

        // grab form and populate it from POST request
        Form<CampaignScenario> submittedForm = form(CampaignScenario.class).bindFromRequest();

        // get the real ID
        Map<String, String> map = submittedForm.data();
        Long id = Long.parseLong(map.get("campaignCon_id"));
        
        //get constraints and save
        CampaignScenario constraintsFromForm = submittedForm.get();
        CampaignScenario constraintsToSave = CampaignScenario.find.byId(id);
        
        
        constraintsToSave.save();
        

        // redirect to index
        //return ok(views.html.globalConstraints.print.render(constraintsFromForm.id));
        return ok(views.html.campaignScenario.view.render(constraintsToSave));

    }
}
