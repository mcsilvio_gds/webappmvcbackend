package models;
 
import play.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

 
@Entity
public class Campaign extends Model{
    
    @Id
    public Long id;
    
    public String name;
    public String description;
    
    @ManyToOne
    public Event event;
        
    public static Finder<Long, Campaign> find = new Finder<Long, Campaign>(
            Long.class, Campaign.class);
    
}
