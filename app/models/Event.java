package models;
 
import java.util.List;

import play.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

 
@Entity
public class Event extends Model {
    
    @Id
    public Long id;
    public String name;
    public String description;
    
    @OneToMany(mappedBy="event", cascade = {CascadeType.ALL})
    public List<Scenario> scenarios;
    
    @OneToMany(mappedBy="event", cascade = {CascadeType.ALL})
    public List<Campaign> campaigns;
    
    public static Finder<Long, Event> find = new Finder<Long, Event>(
            Long.class, Event.class);
    
    
}
