package models;
 
import java.util.List;

import play.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

 
@Entity
public class CampaignScenario extends Model {
    
    @Id
    public Long id;
    
    @ManyToOne
    public Scenario scenario;
    
    @OneToOne
    public Campaign campaign;
    
    public static Finder<Long, CampaignScenario> find = new Finder<Long, CampaignScenario>(
            Long.class, CampaignScenario.class);
    
    
}
