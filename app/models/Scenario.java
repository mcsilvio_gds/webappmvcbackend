package models;
 
import java.util.List;

import play.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

 
@Entity
public class Scenario extends Model{
    
    @Id
    public Long id;
    
    public String name;
    public String description;
    
    @ManyToOne
    public Event event;
    
    @OneToOne(cascade = {CascadeType.ALL})
    public GlobalConstraintSet globalConstraintSet;
    
    @OneToMany(mappedBy="scenario", cascade= {CascadeType.ALL})
    public List<CampaignScenario> campaignScenarios;
     
    
    public static Finder<Long, Scenario> find = new Finder<Long, Scenario>(
            Long.class, Scenario.class);
    
}
