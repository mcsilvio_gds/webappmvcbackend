package models;
 
import play.*;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import javax.persistence.*;

 
@Entity
public class GlobalConstraintSet extends Model{
    
    @Id
    public Long id;
    
    public String name;
    
    @OneToOne(mappedBy="globalConstraintSet")
    public Scenario scenario;
    
    public static Finder<Long, GlobalConstraintSet> find = new Finder<Long, GlobalConstraintSet>(
            Long.class, GlobalConstraintSet.class);
    
}
