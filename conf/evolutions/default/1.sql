# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table campaign (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  event_id                  bigint,
  constraint pk_campaign primary key (id))
;

create table campaign_scenario (
  id                        bigint auto_increment not null,
  scenario_id               bigint,
  campaign_id               bigint,
  constraint pk_campaign_scenario primary key (id))
;

create table event (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  constraint pk_event primary key (id))
;

create table global_constraint_set (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  constraint pk_global_constraint_set primary key (id))
;

create table scenario (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  description               varchar(255),
  event_id                  bigint,
  global_constraint_set_id  bigint,
  constraint pk_scenario primary key (id))
;

alter table campaign add constraint fk_campaign_event_1 foreign key (event_id) references event (id) on delete restrict on update restrict;
create index ix_campaign_event_1 on campaign (event_id);
alter table campaign_scenario add constraint fk_campaign_scenario_scenario_2 foreign key (scenario_id) references scenario (id) on delete restrict on update restrict;
create index ix_campaign_scenario_scenario_2 on campaign_scenario (scenario_id);
alter table campaign_scenario add constraint fk_campaign_scenario_campaign_3 foreign key (campaign_id) references campaign (id) on delete restrict on update restrict;
create index ix_campaign_scenario_campaign_3 on campaign_scenario (campaign_id);
alter table scenario add constraint fk_scenario_event_4 foreign key (event_id) references event (id) on delete restrict on update restrict;
create index ix_scenario_event_4 on scenario (event_id);
alter table scenario add constraint fk_scenario_globalConstraintSet_5 foreign key (global_constraint_set_id) references global_constraint_set (id) on delete restrict on update restrict;
create index ix_scenario_globalConstraintSet_5 on scenario (global_constraint_set_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table campaign;

drop table campaign_scenario;

drop table event;

drop table global_constraint_set;

drop table scenario;

SET FOREIGN_KEY_CHECKS=1;

